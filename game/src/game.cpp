#include <btBulletCollisionCommon.h>
#include "game.h"
#include "bot.h"
#include <stdio.h>

extern "C" {
	#include <luajit-2.1/luajit.h>
	#include <luajit-2.1/lauxlib.h>
	#include <luajit-2.1/lualib.h>
}

void Game::decisionUpdate(btScalar dt) {
	btCollisionObjectArray &collisionObjects = getCollisionObjectArray();
	for (int i = 0, max = collisionObjects.size(); i < max; i++) {
		if (collisionObjects[i]->getUserIndex() != 1) {
			break;
		}
		Bot *bot = static_cast<Bot *>(collisionObjects[i]);
		lua_State *const L = bot->getLuaState();
		lua_createtable(L, 0, 0);
		pushUpdateTable(i);
		bot->queryForTick();
		handleUpdateTable(i);
		bot->luaPop();
	}
}

void Game::update(btScalar dt) {
	decisionUpdate(dt);
	physicsUpdate(dt);
	gameUpdate(dt);
	printOutput();
	printf("0\n");
	frameCleanup();
}

void Game::physicsUpdate(btScalar dt) {
	btCollisionObjectArray &collisionObjects = getCollisionObjectArray();
	for (int i = 0, max = collisionObjects.size(); i < max; i++) {
		if (collisionObjects[i]->getUserIndex() & 1 == 1) {
			KinematicBody *body = static_cast<KinematicBody *>(collisionObjects[i]);
			body->tick(dt);
		}
	}

	performDiscreteCollisionDetection();
	btDispatcher *dispatcher = getDispatcher();
	for (int i = 0, max = dispatcher->getNumManifolds(); i < max; i++) {
		btPersistentManifold *manifold = dispatcher->getManifoldByIndexInternal(i);
		const btCollisionObject *body0 = manifold->getBody0();
		const btCollisionObject *body1 = manifold->getBody1();
		int index0 = -1;
		int index1 = -1;

		auto array = getCollisionObjectArray();
		for (int i = 0, max = array.size(); (index0 == -1 || index1 == -1) && i < max; i++) {
			if (array[i] == body0)
				index0 = i;
			if (array[i] == body1)
				index1 = i;
		}
		if (index0 == -1 || index1 == -1) {
			throw "indices are wrong";
		}

		if (body0->getUserIndex() == 1)
			handleCollision(index0, index1, BODY_ON_BODY);
		else
			handleCollision(index1, index0, BODY_ON_BODY);
	}
	// TODO? rays
}

void Game::addPlayer(int id, const char *const luaCode, const size_t length) {
	Bot *b = new Bot(id, luaCode, length);
	initPlayer(b);
	addCollisionObject(b);
}

Game::Game(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache,
	btCollisionConfiguration *config) : btCollisionWorld(dispatcher, pairCache, config) {}

Game::~Game() {
	btCollisionObjectArray &collisionObjects = getCollisionObjectArray();
	for (int i = 0, max = collisionObjects.size(); i < max; i++) {
		delete(collisionObjects[i]);
	}
	for (int i = 0, max = m_badbots.size(); i < max; i++) {
		delete(m_badbots[i]);
	}
}
