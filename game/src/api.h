#include <btBulletCollisionCommon.h>
extern "C" {
	#include <luajit-2.1/luajit.h>
	#include <luajit-2.1/lauxlib.h>
	#include <luajit-2.1/lualib.h>

	#ifndef EPSILLON
	#define EPSILLON 0.0000001
	#endif

	#ifndef PI
	#define PI 3.141592653589793
	#endif

	void makeVector3(lua_State *L, const btVector3 &vec);
	void makeRotation(lua_State *L, const btQuaternion &rot);
	void makeVectorMetatable(lua_State *L);
	void makeRotationMetatable(lua_State *L);
	int getVector(lua_State *L, btVector3 **a);
	int getVectorPair(lua_State *L, btVector3 **a, btVector3 **b);
	int getRotation(lua_State *L, btQuaternion **a);
	int getRotationPair(lua_State *L, btQuaternion **a, btQuaternion **b);

	int newVector(lua_State *L);
	int rotationFromEuler(lua_State *L);
	int rotationFromAxisAngle(lua_State *L);
	int rotationFromQuaternion(lua_State *L);

	int vector_Cross(lua_State *L);
	int vector_Dot(lua_State *L);
	int vector_Length(lua_State *L);
	int vector_Length2(lua_State *L);
	int vector_Normalize(lua_State *L);
	int vector_Normalized(lua_State *L);
	int vector_ProjectScalar(lua_State *L);
	int vector_ProjectVector(lua_State *L);
	int vector_ProjectOrthogonal(lua_State *L);
	int vector_GetRotation(lua_State *L);

	int vector_X(lua_State *L);
	int vector_Y(lua_State *L);
	int vector_Z(lua_State *L);
	int vector_XYZ(lua_State *L);

	int vector_Add(lua_State *L);
	int vector_Negative(lua_State *L);
	int vector_Subtract(lua_State *L);
	int vector_Equals(lua_State *L);
	int vector_Multiply(lua_State *L);
	int vector_Divide(lua_State *L);

	int vector_ToString(lua_State *L);

	int rotation_Multiply(lua_State *L);
	int rotation_ToEuler(lua_State *L);
	int rotation_ToAxisAngle(lua_State *L);
	int rotation_ToQuaternion(lua_State *L);
}