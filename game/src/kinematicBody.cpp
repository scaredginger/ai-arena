#include <btBulletCollisionCommon.h>
#include <cmath>
#include "kinematicBody.h"

// change velocity with the Heun method
btScalar KinematicBody::getNewVelocity(btScalar dt) const {
	btScalar result1 = f(m_velocity);
	btScalar estimate = m_velocity + dt * result1;
	return m_velocity + dt / 2 * (result1 + f(estimate));
}

btScalar KinematicBody::f(btScalar velocity) const {
	return m_thrust - dragCoefficient * m_velocity * m_velocity;
}

void KinematicBody::tick(btScalar dt) {
	if (m_thrust > maxThrust) {
		m_thrust = maxThrust;
	} else if (m_thrust < 0) {
		m_thrust = 0;
	}

	const btScalar newVel = getNewVelocity(dt);

	const btScalar angle = m_userTorque.getAngle();

	btMatrix3x3 &basis = this->getWorldTransform().getBasis();
	
	const btVector3 originalDirection = basis.getColumn(2);

	btQuaternion originalRotation;
	basis.getRotation(originalRotation);

	const btQuaternion newRotation =  m_userTorque * originalRotation;

	const btScalar ratio = std::abs(angle) < (maxRotation * dt) ? 1 : (maxRotation * dt) / std::abs(angle);
	basis.setRotation(originalRotation.slerp(newRotation, ratio));

	const btVector3 newDirection = basis.getColumn(2);

	const btVector3 displacement = newDirection * (newVel / 2) + originalDirection * (m_velocity / 2);
	this->getWorldTransform().setOrigin(this->getWorldTransform().getOrigin() + displacement * dt);

	m_velocity = newVel;
}

KinematicBody::KinematicBody() : btCollisionObject() {
}

KinematicBody::~KinematicBody() {}
