#include <iostream>
#include <pthread.h>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <endian.h>
#include <malloc.h>
#include <inttypes.h>
#include <btBulletCollisionCommon.h>
#include <unistd.h>
#include "game.h"
#include "basicGame.h"
#include "testGame.h"
#include "gameWithExplosions.h"
#include "bot.h"
extern "C" {
	#include "thread.h"
}

int main(int argc, char **argv) {
	if (prepareThread() != 0) {
		fprintf(stderr, "Thread creation failed");
		return 10;
	}

	char *buffer = (char *)malloc(sizeof(char) * 65536);
	if (buffer == NULL) {
		fprintf(stderr, "failed to allocate");
		return 4;
	}

	srand(time(NULL));

	int gameMode, noTicks, noPlayers;
	if (fgets(buffer, 65536, stdin) == NULL) {
		fprintf(stderr, "Unexpected end of input.\n");
		return 1;
	}
	if (sscanf(buffer, "%d%d%d", &gameMode, &noTicks, &noPlayers) == EOF) {
		fprintf(stderr, "Unexpected end of input.\n");
	}

	btCollisionConfiguration *config = new btDefaultCollisionConfiguration();
	btCollisionDispatcher *dispatcher = new btCollisionDispatcher(config);
	btBroadphaseInterface *pairCache = new btDbvtBroadphase();
	Game *game = NULL;
	switch(gameMode) {
		case 0:
			game = new BasicGame(dispatcher, pairCache, config);
			break;
		case 1:
			game = new TestGame(dispatcher, pairCache, config);
			break;
		case 2:
			game = new GameWithExplosions(dispatcher, pairCache, config);
			break;
		default:
			return 11;
	}
	printf("game: %llu\n", game);
	Bot::loadShape(NULL);

	for (int i = 0; i < noPlayers; i++) {
		if (fgets(buffer, 65536, stdin) == NULL) {
			fprintf(stderr, "Unexpected end of input.\n");
			return 2;
		}
		int id;
		size_t length;
		if (sscanf(buffer, "%d%llu", &id, &length) == EOF) {
			fprintf(stderr, "Unexpected end of input.\n");
			return 3;
		}

		if (fgets(buffer, length, stdin) == NULL) {
			fprintf(stderr, "Unexpected end of input.\n");
			return 5;
		}

		// lua will create an internal copy of buffer, so it's okay to use this
		game->addPlayer(id, buffer, length);
	}
	free(buffer);

	if (noTicks == 0)
		while (!game->hasFinished()) {
			game->update(0.016);
			usleep(16000);
		}
	else
		for (int i = 0; i < noTicks && !game->hasFinished(); i++) {
			game->update(0.016);
			usleep(16000);
		}

	printf("COMPLETE\n");

	delete(game);
	delete(pairCache);
	delete(dispatcher);
	delete(config);
	return 0;
}
