#include <btBulletCollisionCommon.h>

#ifndef KINEMATIC_BODY_H 
#define KINEMATIC_BODY_H

#ifndef EPSILLON
#define EPSILLON 0.0000001
#endif

#ifndef PI
#define PI 3.141592653589793
#endif

class KinematicBody : public btCollisionObject {
protected:
	btScalar m_thrust;
	btScalar m_velocity;
	btQuaternion m_userTorque;

	btScalar f(btScalar velocity) const; // f used for Heun approximation

	constexpr static const btScalar dragCoefficient = 0.05;
	constexpr static const btScalar maxThrust = 1;
	constexpr static const btScalar maxRotation = PI / 3;

public:
	btScalar getNewVelocity(btScalar dt) const;
	void tick(btScalar dt);

	btScalar getUserForce() const {
		return m_thrust;
	}

	btQuaternion getUserTorque() const {
		return m_userTorque;
	}

	btVector3 getLinearVelocity() const {
		return m_velocity * m_worldTransform.getBasis().getColumn(2);
	}

	void setVelocity(btScalar v) {
		m_velocity = v;
	}

	KinematicBody();
	virtual ~KinematicBody();
};

#endif
