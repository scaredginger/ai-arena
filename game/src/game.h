#include <btBulletCollisionCommon.h>
#ifndef GAME_H
#define GAME_H

class Bot;

class Game : public btCollisionWorld {
protected:
	btAlignedObjectArray<Bot *> m_badbots;
	char outputBuffer[32767];
	bool m_hasFinished;

public:
	void update(btScalar dt);
	void physicsUpdate(btScalar dt);
	void decisionUpdate(btScalar dt);
	void badBot();
	void addPlayer(const int id, const char *const luaCode, const size_t length);
	virtual void initPlayer(Bot *bot) {}
	virtual void printOutput() const = 0;
	virtual void pushUpdateTable(const int botIndex) = 0;
	virtual int handleUpdateTable(const int botIndex) = 0;
	virtual void gameUpdate(btScalar dt) = 0;
	virtual void handleCollision(const int index1, const int index2, const int collisionType) = 0;
	virtual void frameCleanup() {}

	bool hasFinished() const {
		return m_hasFinished;
	}

	Game(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config);
	virtual ~Game() noexcept;

	enum collisionTypes {
		BODY_ON_BODY,
	};
};
#endif
