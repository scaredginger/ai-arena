#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <luajit-2.1/lua.h>
#include <luajit-2.1/luajit.h>
#include <luajit-2.1/lauxlib.h>
#include <luajit-2.1/lualib.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>

static enum readyStates {
	INITIALISING = 0,
	ORDER_PREPARED,
	ORDER_COMPLETED
};

typedef struct TfInfo {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	lua_State *L;
	int readyState;
	int nargs;
	int nresults;
	int error;
} TfInfo;

static void cleanup(void *tfi) {
	printf("done cleaning\n");
	TfInfo *tfInfo = (TfInfo *)tfi;
	pthread_mutex_unlock(&tfInfo->mutex);
}

static TfInfo *tf(TfInfo *tfi) {
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	pthread_cleanup_push(cleanup, tfi);

	TfInfo *tfInfo = (TfInfo *)tfi;
	
	for(;;) {
		pthread_mutex_lock(&tfi->mutex);
		while (tfi->readyState != ORDER_PREPARED)
			pthread_cond_wait(&tfi->cond, &tfi->mutex);
		pthread_mutex_unlock(&tfi->mutex);

		tfi->error = lua_pcall(tfInfo->L, tfInfo->nargs, tfInfo->nresults, 0);
		tfi->readyState = ORDER_COMPLETED;
		pthread_mutex_unlock(&tfi->mutex);
		pthread_cond_signal(&tfi->cond);
	}
	pthread_cleanup_pop(1);
	return NULL;
}

/*
void unnecessary() {
	lua_State *states[100];
	for (int i = 0; i < sizeof(states) / sizeof(lua_State *); i++) {
		lua_State *L = luaL_newstate();
		if (L == NULL) {
			return 2;
		}
		luaL_openlibs(L);

		char filename[100];
		sprintf(filename, "test-files/base-%d.lua", i + 1);

		int error = luaL_loadfile(L, filename);
		if (error) {
			fprintf(stderr, "loadfile: %s\n", lua_tostring(L, -1));
			lua_pop(L, 1);
			return 1;
		}

		states[i] = L;
	}
}
*/

static struct TfInfo tfi;
static pthread_t thread;

int prepareThread() {
	tfi.readyState = INITIALISING;
	tfi.L;
	tfi.nargs = 0;
	tfi.nresults = 0;
	tfi.error = 0;
	int error;
	if ((error = pthread_mutex_init(&tfi.mutex, NULL) != 0)) {
		fprintf(stderr, "Failed to create mutex (%d).", error);
		return error;
	}
	if ((error = pthread_cond_init(&tfi.cond, NULL)) != 0) {
		fprintf(stderr, "Failed to create cond (%d).", error);
		return error;
	}
	if ((error = pthread_mutex_lock(&tfi.mutex)) != 0) {
		fprintf(stderr, "Failed to lock mutex (%d).", error);
		return error;
	}

	if (pthread_create(&thread, NULL, tf, &tfi) != 0) {
		perror("pthread_create");
		return -1;
	}
	return 0;
}

// time in nanoseconds
int threadRun(lua_State *const L, const int nargs, const int nresults, int *const luaError, const struct timespec *const time, const int tolerance) {
	tfi.L = L;
	tfi.nargs = nargs;
	tfi.nresults = nresults;
	tfi.readyState = ORDER_PREPARED;

	struct timespec killTime;
	clock_gettime(CLOCK_REALTIME, &killTime);
	killTime.tv_nsec += time->tv_nsec;
	killTime.tv_sec += time->tv_sec;
	if (killTime.tv_nsec >= 1000000000) {
		killTime.tv_nsec -= 1000000000;
		killTime.tv_sec++;
	}

	pthread_mutex_unlock(&tfi.mutex);
	pthread_cond_signal(&tfi.cond);

	pthread_mutex_lock(&tfi.mutex);
	int i;
	for (i = tolerance; i >= 0; i--) {
		int result = 0;
		while(tfi.readyState != ORDER_COMPLETED && result == 0)
			result = pthread_cond_timedwait(&tfi.cond, &tfi.mutex, &killTime);
		pthread_mutex_unlock(&tfi.mutex);
		if (result == 0) {
			break;
		}
		killTime.tv_sec += time->tv_sec;
		killTime.tv_nsec += time->tv_nsec;
		if (killTime.tv_nsec >= 1000000000) {
			killTime.tv_nsec -= 1000000000;
			killTime.tv_sec++;
		}
	}
	if (i < 0) {
		pthread_cancel(thread);
		*luaError = 0;
		prepareThread();
	} else {
		*luaError = tfi.error;
	}
	return i;
}

int destroyThread() {
	pthread_cancel(thread);
	pthread_cond_destroy(&tfi.cond);
	pthread_mutex_destroy(&tfi.mutex);
	return 0;
}
