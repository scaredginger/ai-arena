#include <btBulletCollisionCommon.h>
#include "game.h"
#ifndef GAME_W_EXPLOSIONS_H
#define GAME_W_EXPLOSIONS_H

class GameWithExplosions : public BasicGame {
public:
	typedef int ExplosionEvent;

private:
	btAlignedObjectArray<ExplosionEvent> m_explosions;

public:
	constexpr static const btScalar visualThreshold = 0.5;

	GameWithExplosions(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config);
	virtual void printOutput() const;
	virtual void pushUpdateTable(const int botIndex);
	virtual void handleCollision(const int index1, const int index2, const int collisionType);
	virtual void gameUpdate(btScalar dt);
	virtual void frameCleanup();
};
#endif
