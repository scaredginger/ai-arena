#include <stdio.h>
#include <cstdlib>

#include "basicGame.h"
#include "bot.h"
#include "api.h"

void BasicGame::printOutput() const {
	const btCollisionObjectArray &collisionObjects = getCollisionObjectArray();
	for (int i = 0, max = collisionObjects.size(); i < max; i++) {
		if (collisionObjects[i]->getUserIndex() != 1)
			break;

		const Bot *const bot = static_cast<const Bot *const>(collisionObjects[i]);
		const btTransform &t = bot->getWorldTransform();
		const btVector3 &pos = t.getOrigin();
		const btQuaternion &quat = t.getRotation();
		// format: id position.x position.y position.z quaternion.w quaternion.x quaternion.y quaternion.z
		printf("1 %d %f %f %f %f %f %f %f\n", bot->getId(), pos.x(), pos.y(), pos.z(), quat.w(), quat.x(), quat.y(), quat.z());
	}
}

void BasicGame::addVisualInformation(const Bot *const bot, const Bot *const b) {
	lua_State *L = bot->getLuaState();
	const btVector3 &pos = b->getWorldTransform().getOrigin();
	const btVector3 &position = b->getWorldTransform().getOrigin();

	// adding position to the table
	lua_pushstring(L, "position");
	makeVector3(L, pos);
	lua_rawset(L, -3);

	// adding velocity to the table
	const btVector3 &vel = b->getLinearVelocity();
	lua_pushstring(L, "velocity");
	makeVector3(L, vel);
	lua_rawset(L, -3);

	// adding angular velocity to the table
	const btQuaternion &aVel = b->getUserTorque();
	lua_pushstring(L, "angularVelocity");
	makeRotation(L, aVel);
	lua_rawset(L, -3);
}

void BasicGame::addVisualInformation(const Bot *const bot, const Bot *const b) {
	lua_State *L = bot->getLuaState();
	const btVector3 &pos = b->getWorldTransform().getOrigin();
	const btVector3 &position = b->getWorldTransform().getOrigin();

	// adding relative position to the table
	lua_pushstring(L, "relativeLocalPosition");
	btVector3 relLocalPos = bot->getWorldTransform().getBasis().inverse() * (pos - position).normalized();
	makeVector3(L, relLocalPos);
	lua_rawset(L, -3);

	lua_pushstring(L, "relativeWorldPosition");
	btVector3 relWorldPos = (b->getWorldTransform().getOrigin() - bot->getWorldTransform().getOrigin()).normalized();
	makeVector3(L, relWorldPos);
	lua_rawset(L, -3);
}

void BasicGame::radarAndVisual(const Bot *const bot, const Bot *const b, int *m, int *n) {
	lua_State *L = bot->getLuaState();
	const btVector3 &pos = b->getWorldTransform().getOrigin();
	const btVector3 &position = b->getWorldTransform().getOrigin();
	if ((pos - position).length2() < 900) {
		// create a table for the radar information
		lua_createtable(L, 0, 3);
		addRadarInformation(bot, b);
		// push this to the radar array
		lua_rawseti(L, -4, *n);
		(*n)++;
	} else if ((pos - position).normalized().dot(bot->getLinearVelocity().normalized()) > BasicGame::visualThreshold) {
		// create a table for the visual information
		lua_createtable(L, 0, 2);

		// push the new information (at the top of the stack) to the array
		lua_rawseti(L, -2, *m);
		(*m)++;
	}
}

// assume table is already at top of stack
void BasicGame::pushUpdateTable(int botIndex) {
	btCollisionObjectArray &collisionObjects = getCollisionObjectArray();
	if (collisionObjects[botIndex]->getUserIndex() != 1)
		throw "wtf, botIndex was wrong";
	const Bot *const bot = static_cast<Bot *>(collisionObjects[botIndex]);

	lua_State *L = bot->getLuaState();

	// add a table containing the user's position into the tick table
	lua_pushstring(L, "position");
	const btVector3 &position = bot->getWorldTransform().getOrigin();
	makeVector3(L, position);
	lua_rawset(L, -3);

	lua_pushstring(L, "quaternion");
	makeRotation(L, bot->getWorldTransform().getRotation());
	lua_rawset(L, -3);

	// add a table containing the user's velocity into the tick table
	lua_pushstring(L, "velocity");
	const btVector3 &velocity = bot->getLinearVelocity();
	makeVector3(L, velocity);
	lua_rawset(L, -3);

	// add a table containing radar values
	lua_pushstring(L, "radar");
	lua_createtable(L, collisionObjects.size() - 1, 0);
	int n = 1;
	lua_pushstring(L, "visual");
	lua_createtable(L, collisionObjects.size() - 1, 0);
	int m = 1;
	for (int i = 0; i < botIndex; i++) {
		if (collisionObjects[i]->getUserIndex() != 1) {
			throw "collision object array is wrong";
		}
		const Bot *const b = static_cast<Bot *>(collisionObjects[i]);
		radarAndVisual(bot, b, &m, &n);
	}

	for (int i = botIndex + 1, max = collisionObjects.size(); i < max; i++) {
		if (collisionObjects[i]->getUserIndex() != 1) {
			break;
		}
		const Bot *const b = static_cast<Bot *>(collisionObjects[i]);
		radarAndVisual(bot, b, &m, &n);
	}

	// push visual into the array
	lua_rawset(L, -5);

	// push radar into the array
	lua_rawset(L, -3);
}

int BasicGame::handleUpdateTable(int botIndex) {
	btCollisionObject *tmp = getCollisionObjectArray()[botIndex];
	if (tmp->getUserIndex() != 1) {
		throw "Collision object array was wrong";
	}

	Bot *const bot = static_cast<Bot *>(tmp);
	lua_State *L = bot->getLuaState();

	int tableType = lua_type(L, -1);
	if (tableType == LUA_TNIL) {
		bot->setThrust(0);
		bot->setTorque(btQuaternion::getIdentity());
	} else if (tableType != LUA_TTABLE) {
		fprintf(stderr, "%d bad return from tick()", bot->getId());
		return -1;
	}

	lua_pushstring(L, "thrust");
	lua_rawget(L, -2);
	int forceType = lua_type(L, -1);
	if (forceType == LUA_TNUMBER) {
		btScalar f = lua_tonumber(L, -1);
		bot->setThrust(f);
	} else if (forceType == LUA_TNIL) {
	} else {
		fprintf(stderr, "%d bad force from tick()", bot->getId());
		return -1;
	}

	lua_pushstring(L, "torque");
	lua_rawget(L, -3);
	btQuaternion *torque = (btQuaternion *)luaL_checkudata(L, lua_gettop(L), "rotationMeta");
	if (torque != NULL) {
		bot->setTorque(*torque);
	} else {
		bot->setTorque(btQuaternion::getIdentity());
	}
	lua_pop(L, 2);
}


void BasicGame::gameUpdate(btScalar dt) {
	
}

void BasicGame::handleCollision(const int index1, const int index2, const int collisionType) {
	
}

void BasicGame::initPlayer(Bot *b) {
	b->getWorldTransform().setOrigin(btVector3(static_cast<btScalar>(rand()) / RAND_MAX, static_cast<btScalar>(rand()) / RAND_MAX, static_cast<btScalar>(rand()) / RAND_MAX) * 10);
}

BasicGame::BasicGame(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config) : Game(dispatcher, pairCache, config) {}

BasicGame::~BasicGame() {}
