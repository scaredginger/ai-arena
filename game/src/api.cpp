#include "api.h"
#include <cstdlib>

extern "C" {
	void makeVector3(lua_State *L, const btVector3 &vec) {
		btVector3 *vector = (btVector3 *)lua_newuserdata(L, sizeof(btVector3));
		*vector = vec;
		luaL_getmetatable(L, "vectorMeta");
		lua_setmetatable(L, -2);
	}

	void makeRotation(lua_State *L, const btQuaternion &rot) {
		btQuaternion *q = (btQuaternion *)lua_newuserdata(L, sizeof(btQuaternion));
		*q = rot;
		luaL_getmetatable(L, "rotationMeta");
		lua_setmetatable(L, -2);
	}

	int getVector(lua_State *L, btVector3 **a) {
		*a = (btVector3 *)luaL_checkudata(L, 1, "vectorMeta");
		if (*a == NULL) {
			return -1;
		}
		return 0;
	}

	int getVectorPair(lua_State *L, btVector3 **a, btVector3 **b) {
		*a = (btVector3 *)luaL_checkudata(L, 1, "vectorMeta");
		if (*a == NULL) {
			return -1;
		}
		*b = (btVector3 *)luaL_checkudata(L, 2, "vectorMeta");
		if (*b == NULL) {
			return -1;
		}
		return 0;
	}

	int newVector(lua_State *L) {
		if (!(lua_isnumber(L, 1) && lua_isnumber(L, 1) && lua_isnumber(L, 1))) {
			makeVector3(L, btVector3(0, 0, 0));
			return 1;
		}
		btScalar x = lua_tonumber(L, 1);
		btScalar y = lua_tonumber(L, 2);
		btScalar z = lua_tonumber(L, 3);
		makeVector3(L, btVector3(x, y, z));
		return 1;
	}

	void makeVectorMetatable(lua_State *L) {
		luaL_newmetatable(L, "vectorMeta");

		lua_pushstring(L, "normalize");
		lua_pushcfunction(L, vector_Normalize);
		lua_rawset(L, -3);
		lua_pushstring(L, "normalized");
		lua_pushcfunction(L, vector_Normalized);
		lua_rawset(L, -3);
		lua_pushstring(L, "cross");
		lua_pushcfunction(L, vector_Cross);
		lua_rawset(L, -3);
		lua_pushstring(L, "dot");
		lua_pushcfunction(L, vector_Dot);
		lua_rawset(L, -3);
		lua_pushstring(L, "projectScalar");
		lua_pushcfunction(L, vector_ProjectScalar);
		lua_rawset(L, -3);
		lua_pushstring(L, "projectVector");
		lua_pushcfunction(L, vector_ProjectVector);
		lua_rawset(L, -3);
		lua_pushstring(L, "projectOrthogonal");
		lua_pushcfunction(L, vector_ProjectOrthogonal);
		lua_rawset(L, -3);
		lua_pushstring(L, "getRotation");
		lua_pushcfunction(L, vector_GetRotation);
		lua_rawset(L, -3);
		lua_pushstring(L, "length");
		lua_pushcfunction(L, vector_Length);
		lua_rawset(L, -3);
		lua_pushstring(L, "length2");
		lua_pushcfunction(L, vector_Length2);
		lua_rawset(L, -3);

		lua_pushstring(L, "x");
		lua_pushcfunction(L, vector_X);
		lua_rawset(L, -3);

		lua_pushstring(L, "y");
		lua_pushcfunction(L, vector_Y);
		lua_rawset(L, -3);

		lua_pushstring(L, "z");
		lua_pushcfunction(L, vector_Z);
		lua_rawset(L, -3);

		lua_pushstring(L, "xyz");
		lua_pushcfunction(L, vector_XYZ);
		lua_rawset(L, -3);

		lua_pushstring(L, "__unm");
		lua_pushcfunction(L, vector_Negative);
		lua_rawset(L, -3);

		lua_pushstring(L, "__add");
		lua_pushcfunction(L, vector_Add);
		lua_rawset(L, -3);

		lua_pushstring(L, "__sub");
		lua_pushcfunction(L, vector_Subtract);
		lua_rawset(L, -3);

		lua_pushstring(L, "__mul");
		lua_pushcfunction(L, vector_Multiply);
		lua_rawset(L, -3);

		lua_pushstring(L, "__div");
		lua_pushcfunction(L, vector_Divide);
		lua_rawset(L, -3);

		lua_pushstring(L, "__eq");
		lua_pushcfunction(L, vector_Equals);
		lua_rawset(L, -3);

		lua_pushstring(L, "__tostring");
		lua_pushcfunction(L, vector_ToString);
		lua_rawset(L, -3);

		lua_pushstring(L, "__concat");
		lua_pushcfunction(L, vector_Dot);
		lua_rawset(L, -3);

		lua_pushstring(L, "__index");
		lua_pushvalue(L, -2);
		lua_rawset(L, -3);
		lua_pop(L, 1);
	}

	int getRotation(lua_State *L, btQuaternion **a) {
		*a = (btQuaternion *)luaL_checkudata(L, 1, "rotationMeta");
		if (*a == NULL)
			return -1;
		return 0;
	}

	int rotationFromEuler(lua_State *L) {
		if (!(lua_isnumber(L, 1) && lua_isnumber(L, 2) && lua_isnumber(L, 3))) {
			return 0;
		}
		btScalar x = lua_tonumber(L, 1);
		btScalar y = lua_tonumber(L, 2);
		btScalar z = lua_tonumber(L, 3);
		makeRotation(L, btQuaternion(y, x, z));
		return 1;
	}

	int rotationFromAxisAngle(lua_State *L) {
		btVector3 *axis;
		btScalar angle;
		if (!lua_isnumber(L, 2)) {
			return 0;
		}
		getVector(L, &axis);
		angle = lua_tonumber(L, 2);
		makeRotation(L, btQuaternion(*axis, angle));
		return 1;
	}

	int rotationFromQuaternion(lua_State *L) {
		if (!(lua_isnumber(L, 1) && lua_isnumber(L, 2) && lua_isnumber(L, 3) && lua_isnumber(L, 4)))
			return 0;
		btScalar w = lua_tonumber(L, 1);
		btScalar x = lua_tonumber(L, 2);
		btScalar y = lua_tonumber(L, 3);
		btScalar z = lua_tonumber(L, 4);
		makeRotation(L, btQuaternion(x, y, z, w));
		return 1;
	}

	int rotation_ToEuler(lua_State *L) {
		btQuaternion *a;
		if (getRotation(L, &a) != 0) {
			return 0;
		}
		btScalar x, y, z;
		btMatrix3x3(*a).getEulerZYX(z, y, x);
		lua_pushnumber(L, x);
		lua_pushnumber(L, y);
		lua_pushnumber(L, z);
		return 3;
	}

	int rotation_ToAxisAngle(lua_State *L) {
		btQuaternion *a;
		if (getRotation(L, &a) != 0) {
			return 0;
		}
		makeVector3(L, a->getAxis());
		lua_pushnumber(L, a->getAngle());
		return 2;
	}

	int rotation_ToQuaternion(lua_State *L) {
		btQuaternion *a;
		if (getRotation(L, &a) != 0) {
			return 0;
		}
		lua_pushnumber(L, a->w());
		lua_pushnumber(L, a->x());
		lua_pushnumber(L, a->y());
		lua_pushnumber(L, a->z());
		return 4;
	}

	int rotation_Multiply(lua_State *L) {
		btQuaternion *a = (btQuaternion *)luaL_checkudata(L, 1, "rotationMeta");
		btQuaternion *b = (btQuaternion *)luaL_checkudata(L, 2, "rotationMeta");
		if (b == NULL) {
			btVector3 *v = (btVector3 *)luaL_checkudata(L, 2, "vectorMeta");
			if (v == NULL) {
				return 0;
			}
			makeVector3(L, v->rotate(a->getAxis(), a->getAngle()));
			return 1;
		}
		makeRotation(L, *a * *b);
		return 1;
	}

	void makeRotationMetatable(lua_State *L) {
		luaL_newmetatable(L, "rotationMeta");

		lua_pushstring(L, "__multiply");
		lua_pushcfunction(L, rotation_Multiply);
		lua_rawset(L, -3);

		lua_pushstring(L, "toEuler");
		lua_pushcfunction(L, rotation_ToEuler);
		lua_rawset(L, -3);

		lua_pushstring(L, "toAxisAngle");
		lua_pushcfunction(L, rotation_ToAxisAngle);
		lua_rawset(L, -3);

		lua_pushstring(L, "toQuaternion");
		lua_pushcfunction(L, rotation_ToQuaternion);
		lua_rawset(L, -3);

		lua_pushstring(L, "__index");
		lua_pushvalue(L, -2);
		lua_rawset(L, -3);
		lua_pop(L, 1);
	}

	int vector_Cross(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0) {
			return 0;
		}

		btVector3 product = a->cross(*b);
		makeVector3(L, product);

		return 1;
	}
	
	int vector_Dot(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0)
			return 0;

		btScalar product = a->dot(*b);

		lua_pushnumber(L, product);
		return 1;
	}
	
	int vector_Normalize(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0) {
			return 0;
		}
		a->normalize();

		makeVector3(L, *a);
		return 1;
	}

	int vector_Normalized(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0) {
			return 0;
		}
		btVector3 c = a->normalized();

		makeVector3(L, c);
		return 1;
	}

	int vector_ProjectScalar(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0)
			return 0;

		if (b->length2() <= EPSILLON) {
			lua_pushnumber(L, 0);
			return 1;
		}
		b->normalize();

		lua_pushnumber(L, a->dot(*b));
		return 1;
	}

	int vector_ProjectVector(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0) {
			return 0;
		}

		if (b->length2() <= EPSILLON) {
			btVector3 zero(0, 0, 0);
			makeVector3(L, zero);
			return 1;
		}
		b->normalize();

		btVector3 projection = *a - a->dot(*b) * *b;

		makeVector3(L, projection);
		return 1;
	}

	int vector_ProjectOrthogonal(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0) {
			return 0;
		}

		if (b->length2() <= EPSILLON) {
			btVector3 zero(0, 0, 0);
			makeVector3(L, zero);
			return 1;
		}
		b->normalize();

		btVector3 projection = *a - a->dot(*b) * *b;
		makeVector3(L, projection);
		return 1;
	}

	int vector_GetRotation(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0) {
			return -1;
		}

		a->normalize();
		b->normalize();

		btScalar angle = acos(b->dot(*a));

		if (btFabs(angle) < EPSILLON) {
			makeRotation(L, btQuaternion::getIdentity());
			return 1;
		} else if (btFabs(PI - angle) < EPSILLON) {
			btVector3 half(PI, 0, 0);
			makeRotation(L, btQuaternion::getIdentity());
			return 1;
		}

		btVector3 axis = a->cross(*b).normalized();

		makeRotation(L, btQuaternion(axis, angle));
		return 1;
	}

	int vector_Add(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0)
			return 0;
		btVector3 c = *a + *b;
		makeVector3(L, c);
		return 1;
	}

	int vector_Negative(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		btVector3 c = -(*a);
		makeVector3(L, c);
		return 1;
	}

	int vector_Subtract(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0)
			return 0;
		btVector3 c = *a - *b;
		makeVector3(L, c);
		return 1;
	}

	int vector_Equals(lua_State *L) {
		btVector3 *a, *b;
		if (getVectorPair(L, &a, &b) != 0)
			return 0;
		if (btFabs(a->x() - b->x()) > EPSILLON) {
			lua_pushboolean(L, false);
			return 1;
		}
		if (btFabs(a->y() - b->y()) > EPSILLON) {
			lua_pushboolean(L, false);
			return 1;
		}
		if (btFabs(a->z() - b->z()) > EPSILLON) {
			lua_pushboolean(L, false);
			return 1;
		}
		lua_pushboolean(L, true);
		return 1;
	}

	int vector_Multiply(lua_State *L) {
		if (lua_isnumber(L, 1)) {
			btVector3 *a = (btVector3 *)luaL_checkudata(L, 2, "vectorMeta");
			if (a == NULL) {
				return 0;
			}
			btScalar multiplier = lua_tonumber(L, 1);
			btVector3 c = *a * multiplier;
			makeVector3(L, c);
			return 1;
		} else if (lua_isnumber(L, 2)) {
			btVector3 *a = (btVector3 *)luaL_checkudata(L, 1, "vectorMeta");
			if (a == NULL) {
				return 0;
			}
			btScalar multiplier = lua_tonumber(L, 2);
			btVector3 c = *a * multiplier;
			makeVector3(L, c);
			return 1;
		}
		return 0;
	}

	int vector_Divide(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		if (!lua_isnumber(L, 2)) {
			return 0;
		}
		btVector3 c = *a / lua_tonumber(L, 2);
		makeVector3(L, c);
		return 1;
	}

	int vector_Length(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->length());
		return 1;
	}

	int vector_Length2(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->length2());
		return 1;
	}

	int vector_ToString(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		char buffer[100];
		sprintf(buffer, "(%f, %f, %f)", a->x(), a->y(), a->z());
		lua_pushstring(L, buffer);
		return 1;
	}

	int vector_X(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->x());
		return 1;
	}

	int vector_Y(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->x());
		return 1;
	}

	int vector_Z(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->x());
		return 1;
	}

	int vector_XYZ(lua_State *L) {
		btVector3 *a;
		if (getVector(L, &a) != 0)
			return 0;
		lua_pushnumber(L, a->x());
		lua_pushnumber(L, a->y());
		lua_pushnumber(L, a->z());
		return 3;
	}
}
