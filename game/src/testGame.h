#include "basicGame.h"
#include "bot.h"

class TestGame : public BasicGame {
public:
	virtual void handleCollision(const int index1, const int index2, const int collisionType);
	virtual void initPlayer(Bot *b) {
		if (b->getId() == 2) {
			b->getWorldTransform().setOrigin(btVector3(0, 0, 10));
			b->getWorldTransform().setRotation(btQuaternion(3.141592653589793, 0, 0));
		} else {
			b->getWorldTransform().setOrigin(btVector3(0, 0, -10));
		}
	}
	TestGame(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config) : BasicGame(dispatcher, pairCache, config) {}
	virtual ~TestGame() {}
};
