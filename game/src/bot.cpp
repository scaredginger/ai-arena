#include "bot.h"
#include <cmath>
#include <chrono>
#include "api.h"
#include <stdio.h>

extern "C" {
	#include <luajit-2.1/luajit.h>
	#include <luajit-2.1/lauxlib.h>
	#include <luajit-2.1/lualib.h>
	#include "thread.h"
}

#ifndef PI
#define PI 3.141592653589793
#endif

#ifndef EPSILLON
#define EPSILLON 0.0000001
#endif

btCollisionShape *Bot::shape;

void Bot::loadShape(const char *filename) {
	Bot::shape = new btSphereShape(1);
}

void Bot::destroyShape() {
	delete(Bot::shape);
}

void Bot::createEnv() {
	lua_newtable(m_L);
	addGlobalFunction("print");
	addGlobalFunction("assert");
	addGlobalFunction("error");
	addGlobalFunction("ipairs");
	addGlobalFunction("pairs");
	addGlobalFunction("getmetatable");
	addGlobalFunction("next");
	addGlobalFunction("rawequal");
	addGlobalFunction("rawget");
	addGlobalFunction("rawset");
	addGlobalFunction("setmetatable");
	addGlobalFunction("tonumber");
	addGlobalFunction("tostring");
	addGlobalFunction("type");
	addGlobalFunction("unpack");

	openModule("table");
	addModuleFunction("concat");
	addModuleFunction("insert");
	addModuleFunction("maxn");
	addModuleFunction("remove");
	addModuleFunction("sort");
	closeModule();

	openModule("math");
	addModuleFunction("abs");
	addModuleFunction("acos");
	addModuleFunction("asin");
	addModuleFunction("atan");
	addModuleFunction("atan2");
	addModuleFunction("ceil");
	addModuleFunction("cos");
	addModuleFunction("cosh");
	addModuleFunction("deg");
	addModuleFunction("exp");
	addModuleFunction("floor");
	addModuleFunction("fmod");
	addModuleFunction("frexp");
	addModuleFunction("huge");
	addModuleFunction("ldexp");
	addModuleFunction("log");
	addModuleFunction("log10");
	addModuleFunction("max");
	addModuleFunction("min");
	addModuleFunction("modf");
	addModuleFunction("pi");
	addModuleFunction("pow");
	addModuleFunction("rad");
	addModuleFunction("random");
	addModuleFunction("sin");
	addModuleFunction("sinh");
	addModuleFunction("sqrt");
	addModuleFunction("tan");
	addModuleFunction("tanh");
	closeModule();

	lua_pushstring(m_L, "newVector");
	lua_pushcfunction(m_L, newVector);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "rotationFromEuler");
	lua_pushcfunction(m_L, rotationFromEuler);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "rotationFromAxisAngle");
	lua_pushcfunction(m_L, rotationFromAxisAngle);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "rotationFromQuaternion");
	lua_pushcfunction(m_L, rotationFromQuaternion);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "vectorRotation");
	lua_pushcfunction(m_L, vector_GetRotation);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "normalized");
	lua_pushcfunction(m_L, vector_Normalized);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "projectScalar");
	lua_pushcfunction(m_L, vector_ProjectScalar);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "projectVector");
	lua_pushcfunction(m_L, vector_ProjectVector);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "projectOrthogonal");
	lua_pushcfunction(m_L, vector_ProjectOrthogonal);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "cross");
	lua_pushcfunction(m_L, vector_Cross);
	lua_rawset(m_L, -3);

	lua_pushstring(m_L, "dot");
	lua_pushcfunction(m_L, vector_Dot);
	lua_rawset(m_L, -3);
}

// precondition: update info is at top of stack
int Bot::queryForTick() {
	lua_pushstring(m_L, "tick");
	lua_rawget(m_L, -3);
	if (!lua_isfunction(m_L, -1)) {
		fprintf(stderr, "%d (tick() was not a function)", m_id);
		return -1;
	}

	// put function below update info in the stack
	lua_insert(m_L, lua_gettop(m_L) - 1);

	timespec timeGiven;
	timeGiven.tv_nsec = 1000000;
	timeGiven.tv_sec = 0;

	timespec s;
	clock_gettime(CLOCK_REALTIME, &s);

	int luaError = 0;
	int error = threadRun(m_L, 1, 1, &luaError, &timeGiven, 5);
	if (luaError != 0) {
		fprintf(stderr, "%d problem calling tick(): %s\n", m_id, lua_tostring(m_L, -1));
		lua_pop(m_L, 1);
		return -1;
	}
	if (error < 0) {
		timespec e;
		clock_gettime(CLOCK_REALTIME, &e);
		fprintf(stderr, "%d took too long calling tick() %d \n", (e.tv_sec - s.tv_sec) * 1000000000 + (e.tv_nsec - s.tv_nsec));
		return -1;
	} else if (error < 1) {
		permittedNaughtiness -= 1 - error;
		if (permittedNaughtiness <= 0) {
			fprintf(stderr, "%d was too naughty\n", m_id);
			return -1;
		}
	}
	// setForcesFromTick();
	// lua_pop(m_L, 1);
}

/*
int Bot::queryForStart() {
	lua_pushstring(m_L, "start");
	lua_rawget(m_L, -2);
	if (!lua_isfunction(m_L, -1)) {
		fprintf(stderr, "%d (start() was not a function)\n", m_id);
		return -1;
	}
	auto t = getWorldTransform().getOrigin();
	makeVector3(m_L, t);

	timespec timeGiven;
	timeGiven.tv_nsec = 1000000;
	timeGiven.tv_sec = 0;

	int luaError = 0;
	int error = threadRun(m_L, 0, 0, &luaError, &timeGiven, 0);
	if (error < 0) {
		fprintf(stderr, "bad bot %d\n", m_id);
		return -1;
	}
	if (luaError) {
		fprintf(stderr, "calling lua: %d (%s)\n", m_id, lua_tostring(m_L, -1));
		lua_pop(m_L, 1);
		return -1;
	}
	setForcesFromTick();
	lua_pop(m_L, 1);
	return 0;
}
*/

void Bot::initCollisionObject() {
	m_velocity = 0;
	m_thrust = 0;
	m_userTorque = btQuaternion::getIdentity();
	this->setCollisionShape(shape);
}

Bot::Bot(const int id, const char * const luaCode, const size_t length) : m_id(id), permittedNaughtiness(3), KinematicBody() {
	this->initCollisionObject();
	m_userIndex = 1;

	m_L = luaL_newstate();   /* opens Lua */
	int tolerance = 0;
	int luaError = 0;
	if (m_L == NULL) {
		throw "Failed initialising Lua";
	}
	luaL_openlibs(m_L);

	makeVectorMetatable(m_L);
	makeRotationMetatable(m_L);

	luaError = luaL_loadstring(m_L, luaCode);
	if (luaError) {
		fprintf(stderr, "loadfile: %s\n", lua_tostring(m_L, -1));
		lua_pop(m_L, 1);  /* pop error message from the stack */
		goto closeLua;
	}
	createEnv();
	if (lua_setfenv(m_L, -2) == 0) {
		printf("Bad setfenv\n");
		goto closeLua;
	}

	timespec timeGiven;
	timeGiven.tv_nsec = 1000000;
	timeGiven.tv_sec = 0;

	tolerance = threadRun(m_L, 0, 1, &luaError, &timeGiven, 0);
	if (tolerance < 0) {
		fprintf(stderr, "bad bot %d\n", id);
		goto closeLua;
	}
	if (luaError) {
		fprintf(stderr, "calling lua: %d (%s)\n", id, lua_tostring(m_L, -1));
		lua_pop(m_L, 1);  /* pop error message from the stack */
		goto closeLua;
	}
	return;

	closeLua:
		lua_close(m_L);
		throw "Something bad happened.";
}

Bot::~Bot() {
	lua_close(m_L);
}
