#include "bot.h"
#include "testGame.h"
#include <stdio.h>

void TestGame::handleCollision(const int index1, const int index2, const int collisionType) {
	const btCollisionObjectArray &arr = getCollisionObjectArray();
	const Bot *const bot1 = static_cast<const Bot *const>(arr[index1]);
	const Bot *const bot2 = static_cast<const Bot *const>(arr[index2]);
	printf("%d collided with %d\n", bot1->getId(), bot2->getId());
}
