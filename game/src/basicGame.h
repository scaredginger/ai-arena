#include "game.h"
#ifndef BASIC_GAME_H
#define BASIC_GAME_H

class BasicGame : public Game {
public:
	constexpr static const btScalar visualThreshold = 0.5;

	BasicGame(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config);
	virtual ~BasicGame();
	virtual void printOutput() const;
	virtual void pushUpdateTable(const int botIndex);
	virtual int handleUpdateTable(const int botIndex);
	virtual void gameUpdate(btScalar dt);
	virtual void handleCollision(const int index1, const int index2, const int collisionType);
	virtual void initPlayer(Bot *bot);

	void radarAndVisual(const Bot *const bot, const Bot *const b, int *m, int *n);
	virtual void addRadarInformation(const Bot *const bot, const Bot *const b);
	virtual void addVisualInformation(const Bot *const bot, const Bot *const b);
};
#endif
