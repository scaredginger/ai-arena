#include <luajit-2.1/lua.h>
#include <time.h>

int prepareThread();
int threadRun(lua_State *L, int nargs, int nresults, int *luaError, struct timespec *time, int tolerance);
int destroyThread();
