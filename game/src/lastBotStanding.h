#include "./basicGame.h"
#ifndef LAST_BOT_STANDING_H
#define LAST_BOT_STANDING_H

class LastBotStanding : public BasicGame {
private:


public:
    static const int startingHealth = 1000;

	LastBotStanding(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config);
	virtual ~LastBotStanding();
	virtual void printOutput() const;
	virtual void pushUpdateTable(const int botIndex);
	virtual int handleUpdateTable(const int botIndex);
	virtual void gameUpdate(btScalar dt);
	virtual void handleCollision(const int index1, const int index2, const int collisionType);
	virtual void initPlayer(Bot *bot);

	virtual void addRadarInformation(const Bot *const bot, const Bot *const b);
	virtual void addVisualInformation(const Bot *const bot, const Bot *const b);
};

#endif