#include <btBulletCollisionCommon.h>
#include "basicGame.h"
#include "bot.h"
#include "api.h"
#include <stdio.h>
#include <cstdlib>
#include "gameWithExplosions.h"

GameWithExplosions::GameWithExplosions(btCollisionDispatcher *dispatcher, btBroadphaseInterface *pairCache, btCollisionConfiguration *config) : m_explosions(), BasicGame(dispatcher, pairCache, config) {}

void GameWithExplosions::printOutput() const {
	BasicGame::printOutput();
	int max = m_explosions.size();
	auto arr = getCollisionObjectArray();
	if (max > 0) {
		printf("2 %d", static_cast<Bot *>(arr[m_explosions[0]])->getId());
		for (int i = 1; i < max; i++) {
			printf(" %d", static_cast<Bot *>(arr[m_explosions[i]])->getId());
		}
		printf("\n");
	}
}

void GameWithExplosions::frameCleanup() {
	m_explosions.resize(0);
}

void GameWithExplosions::pushUpdateTable(const int botIndex) {
	BasicGame::pushUpdateTable(botIndex);
	const btCollisionObject *const tmp = getCollisionObjectArray()[botIndex];
	const Bot *const bot = static_cast<const Bot *const>(tmp);
	lua_State *L = bot->getLuaState();
	lua_pushstring(L, "explosions");

	int max = m_explosions.size();
	lua_createtable(L, max, 0);

	for (int i = 0; i < max; i++) {
		lua_pushnumber(L, m_explosions[i]);
		lua_rawseti(L, -2, i + 1);
	}
	lua_rawset(L, -3);
}

void GameWithExplosions::handleCollision(const int index1, const int index2, const int collisionType) {
	auto arr = getCollisionObjectArray();
	if (arr[index1]->getUserIndex() == 1)
		m_explosions.push_back(index1);
	if (arr[index2]->getUserIndex() == 1)
		m_explosions.push_back(index2);
}

void GameWithExplosions::gameUpdate(btScalar dt) {
	btCollisionObjectArray &arr = getCollisionObjectArray();
	int maxIndex = arr.size();
	for (int i = 0, max = m_explosions.size(); i < max; i++) {
		int index = m_explosions[i];
		if (index >= maxIndex) {
			throw "indices were wrong";
		}
		if (arr[index]->getUserIndex() == 1) {
			Bot *b = static_cast<Bot *>(arr[index]);
			b->getWorldTransform()
				.setOrigin(10 * btVector3(
					static_cast<btScalar>(rand()) / RAND_MAX,
					static_cast<btScalar>(rand()) / RAND_MAX,
					static_cast<btScalar>(rand()) / RAND_MAX
				));
			b->getWorldTransform().setRotation(btQuaternion::getIdentity());
			b->setVelocity(0);
		}
	}
}
