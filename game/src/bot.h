#ifndef BOT_H
#define BOT_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
extern "C" {
	#include <luajit-2.1/lua.h>
}
#include <time.h>
#include "kinematicBody.h"

class Bot : public KinematicBody {
protected:
	static btCollisionShape *shape;

	char m_displayName[44];
	char m_creator[40];
	int m_id;
	int health;
	int permittedNaughtiness;
	lua_State *m_L;

	int timeoutsRemaining;

public:
	static void loadShape(const char *filename);
	static void destroyShape();

	void addGlobalFunction(const char *name) {
		lua_pushstring(m_L, name);
		lua_getglobal(m_L, name);
		lua_rawset(m_L, -3);
	}

	void openModule(const char *name) {
		lua_pushstring(m_L, name);
		lua_getglobal(m_L, name);
	}

	void addModuleFunction(const char *name) {
		lua_pushstring(m_L, name);
		lua_pushstring(m_L, name);
		lua_gettable(m_L, -3);
		lua_rawset(m_L, -5);
	}

	void setThrust(const btScalar thrust) {
		m_thrust = thrust;
	}

	void setTorque(const btQuaternion &torque) {
		m_userTorque = torque;
	}

	void closeModule() {
		lua_pop(m_L, 2);
	}

	int getId() const {
		return m_id;
	}

	void createEnv();

	lua_State *getLuaState() const {
		return m_L;
	}

	void luaPop() const {
		lua_pop(m_L, 1);
	}

	// int queryForStart() const;

	// void tableFromUpdateInfo(const UpdateInfo &updateInfo) const;

	// void setForcesFromTick();

	int queryForTick();

	void initCollisionObject();

	Bot(const int id, const char *const luaCode, const size_t length);
	virtual ~Bot();
};
#endif
