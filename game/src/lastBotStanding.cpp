#include "./lastBotStanding.h"

void LastBotStanding::pushUpdateTable(const int botIndex) {
    BasicGame::pushUpdateTable(botIndex);
    // push current health
    // push most recent hits
    // fix positions
}

void LastBotStanding::printOutput() const {
    // print deaths in last frame
    // print hits in last frame 
    // print current healths
}

int LastBotStanding::handleUpdateTable(const int botIndex) {
    // handle weapon inputs
}

void LastBotStanding::gameUpdate(const btScalar dt) {
    // fire shots when needed
}

void LastBotStanding::handleCollision(const int index1, const int index2, const int collisionType) {
    // damage the bots accordingly
    // add this information to a queue to be printed later
}

void LastBotStanding::initPlayer(Bot *bot) {
    // set health to default value
}

void LastBotStanding::addRadarInformation(const Bot *const bot, const Bot *const b) {
    // rules on health to be determined
}

void LastBotStanding::addVisualInformation(const Bot *const bot, const Bot *const b) {
    // rules on health to be determined
}