const WebSocket = require('uws');

let scheduler = null;

const sockets = [];

function onMessage(buffer) {
  if (buffer instanceof ArrayBuffer) {
    this.handleBinary(buffer);
  } else if (typeof buffer === 'string') {
    this.handleString(buffer);
  }
}

function onClose() {
  if (this.game) {
    this.game.removeSubscriber(this.id);
    delete sockets[this.id];
  }
}

function handleString(str) {
  let obj = null;
  try {
    obj = JSON.parse(str);
  } catch (e) {
    console.error(e);
    this.send('{"success": false}');
    return;
  }
  switch (obj.msgType) {
    case 'sub':
      (() => {
        const gameId = parseInt(obj.gameId, 10);
        this.send(`{"gameId": ${gameId}, "success": ${this.subscribe(gameId)}}`);
      })();
      break;
    case 'unsub':
      this.unsubscribe();
      break;
    default:
      break;
  }
}

function handleBinary(buffer) {
  const view = new DataView(buffer);
  const msgType = view.getUint8(0);
  switch (msgType) {
    case 1:
      (() => {
        const gameId = view.getUint32(1);
        this.subscribe(gameId);
      })();
      break;
    case 2:
      this.unsubscribe();
      break;
    default:
      break;
  }
}

function subscribe(gameId) {
  if (this.game) {
    this.unsubscribe();
  }
  if (scheduler.games[gameId]) {
    scheduler.games[gameId].addSubscriber(this);
    this.game = scheduler.games[gameId];
  }
}

function unsubscribe() {
  if (this.game) {
    this.game.removeSubscriber(this);
  }
}

module.exports = {
  createWebsocketServer(s) {
    scheduler = s;
    const ws = new WebSocket.Server({ port: 3001 });
    /* eslint-disable no-param-reassign */
    ws.on('connection', (conn) => {
      conn.handleBinary = handleBinary.bind(conn);
      conn.handleString = handleString.bind(conn);
      conn.on('message', onMessage.bind(conn));
      conn.on('close', onClose.bind(conn));
      conn.subscribe = subscribe.bind(conn);
      conn.unsubscribe = unsubscribe.bind(conn);

      let id = Math.floor(Math.random() * 1000000000);
      while (typeof sockets[id] !== 'undefined') {
        id = Math.floor(Math.random() * 1000000000);
      }
      conn.id = id;
      sockets[id] = conn;
    });
    /* eslint-enable no-param-reassign */
    return ws;
  },
};
