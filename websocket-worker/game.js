const cp = require('child_process');
const fs = require('fs');

class Game {
  constructor(gameOptions) {
    this.gameOptions = Object.assign({}, gameOptions);
    const proc = cp.spawn('./game', [], {
      stdio: 'pipe',
    });
    let str = '';
    str += `${gameOptions.gameMode} ${gameOptions.noTicks} ${gameOptions.bots.length}`;
    this.gameOptions.bots = gameOptions.bots.filter(bot => (
      bot.lua && bot.lua.length < 65535 && bot.lua.indexOf('\n') === -1
    ));
    str += gameOptions.bots.map(bot => `${bot.id} ${bot.lua.length}\n${bot.lua}`);

    proc.stdin.write(str);

    this.fileStream = fs.createWriteStream(`games/${gameOptions.id}.txt`);
    this.gameOptions = gameOptions;
    this.proc = proc;
    this.listeningSockets = [];
    proc.stdout.on('readable', this.onReadable.bind(this));
    proc.on('exit', this.onEnd);
  }

  addSubscriber(socket) {
    this.listeningSockets[socket.id] = socket;
  }

  removeSubscriber(socket) {
    delete (this.listeningSockets[socket.id]);
  }

  onReadable() {
    const data = this.proc.read();
    this.fileStream.write(data);
    this.handleData(data);
  }

  writeToFile(str) {
    this.fileStream.write(str);
  }

  broadcast(msg) {
    this.listeningSockets.forEach((socket) => {
      if (socket) {
        socket.send(msg);
      }
    });
  }

  onEnd() {
    this.listeningSockets.forEach((socket) => {
      socket.close();
    });
  }
}

class GameWithExplosions extends Game {
  constructor(gameOptions) {
    super(gameOptions);
    this.frameData = [];
    this.explosions = [];
  }

  handleData(data) {
    data.split('\n').forEach((datum) => {
      const msg = datum.split(' ');
      const type = parseInt(msg[0], 10);
      switch (type) {
        case 0:
          this.broadcastFrame();
          break;

        case 1:
          this.pushPositionUpdate(msg);
          break;

        case 2:
          this.pushExplosions(msg);
          break;

        default:
          break;
      }
    });
  }

  pushPositionUpdate(msg) {
    this.frameData.push(msg);
  }

  pushExplosions(msg) {
    this.explosions.push(msg);
  }

  getFrameLength() {
    // eslint-disable-next-line no-mixed-operators
    return (4 * 7 + 4) * this.frameData.length + 4 * this.explosions.length;
  }

  allocateFrame() {
    return new ArrayBuffer(this.getFrameLength());
  }

  /* eslint-disable no-mixed-operators */
  prepareFrame(view) {
    view.setUint8(0, 1);
    view.setUint8(1, this.frameData.length);
    view.setUint8(2, this.explosions.length);
    for (let i = 0; i < this.frameData.length; i++) {
      view.setInt32(
        3 + i * (7 * 4 + 4),
        parseInt(this.frameData[i][1], 10),
      );
      for (let j = 2; i < this.frameData[i].length; j++) {
        view.setFloat32(
          3 + i * (7 * 4 + 4) + (j - 1) * 4 + 4,
          parseFloat(this.frameData[i][j]),
          false,
        );
      }
    }
    const offset = 3 + this.frameData.length * (7 * 4 + 4);
    for (let i = 1; i < this.explosions.length; i++) {
      view.setInt32(offset + i * 4, parseInt(this.explosions[i], 10));
    }
  }
  /* eslint-enable no-mixed-operators */

  broadcastFrame() {
    const buffer = this.allocateFrame();
    const view = new DataView(buffer);
    this.prepareFrame(view);
    this.broadcast(buffer);
  }
}

module.exports = {
  Game,
  GameWithExplosions,
};
