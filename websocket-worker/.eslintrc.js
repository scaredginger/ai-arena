module.exports = {
	"extends": "airbnb-base",
	"parserOptions": {
		"sourceType": "module"
	},
	"rules": {
		"no-plusplus": 0,
		"no-console": 0,
		"function-paren-newline": 0,
		"object-curly-spacing": ["error", "always"],
	}
};
