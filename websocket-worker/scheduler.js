const { Game } = require('./game');

const tls = false;
const http = tls ? require('https') : require('http');

const apiServer = {
  hostname: 'localhost',
  port: 3000,
};

const credentials = {
  username: 'local server',
  password: 'tmp',
};

const loginInfo = {
  id: -1,
  token: '',
};

const games = [];
const noConcurrentGames = 3;

function login() {
  return new Promise((resolve, reject) => {
    const options = Object.assign({}, apiServer);
    options.method = 'POST';
    options.headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    options.path = '/api/NormalUsers/login';
    const req = http.request(options, (response) => {
      if (response.statusCode !== 200) {
        reject(response.statusCode);
        return;
      }
      let resString = '';
      response.on('data', (chunk) => {
        resString += chunk;
      });
      response.on('end', () => {
        const resObject = JSON.parse(resString);
        resolve(resObject);
      });
    });
    req.on('error', (e) => {
      reject(e);
    });
    req.write(JSON.stringify(credentials));
    req.end();
  });
}

function fetchGames() {
  return new Promise((resolve, reject) => {
    const options = Object.assign({}, apiServer);
    options.method = 'POST';
    options.headers = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    };
    options.path = `/api/QueuedGames/serve?accessToken=${loginInfo.token}`;
    const req = http.request(options, (response) => {
      if (response.statusCode !== 200) {
        reject(response.statusCode);
        return;
      }
      let resString = '';
      response.on('data', (chunk) => {
        resString += chunk;
      });
      response.on('end', () => {
        const resObject = JSON.parse(resString);
        resolve(resObject.gameList);
      });
    });
    req.on('error', (e) => {
      reject(e);
    });
    req.write(JSON.stringify({
      accepting: games.length < noConcurrentGames,
    }));
    req.end();
  });
}

function refreshToken() {
  return login().then((obj) => {
    console.log(obj);
    const { id, userId, ttl } = obj;
    setTimeout(() => {
      refreshToken().catch((e) => {
        console.error(e);
        process.exit(-1);
      });
    }, (ttl - 60) * 1000);
    loginInfo.token = id;
    loginInfo.id = userId;
  });
}

function pollServer() {
  return fetchGames().then((gameList) => {
    gameList.forEach((gameInfo) => {
      games[gameInfo.id] = new Game(gameInfo);
    });
  });
}

module.exports = {
  createScheduler() {
    refreshToken().then(
      pollServer,
    ).catch((e) => {
      console.error(e);
      process.exit(-1);
    });
    setInterval(() => {
      pollServer().catch((e) => {
        console.error(e);
        process.exit(-1);
      });
    }, 60 * 1000);
    return {
      games,
    };
  },
};
