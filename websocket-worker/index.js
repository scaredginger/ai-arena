const { createScheduler } = require('./scheduler');
const { createWebsocketServer } = require('./server');

createWebsocketServer(createScheduler());
