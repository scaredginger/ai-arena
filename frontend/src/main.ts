import Vue from 'vue';
import Vuetify from 'vuetify';
import '../node_modules/vuetify/dist/vuetify.css';
import App from './App.vue';
import router from './router';
import store from './store';

(() => {
  const login = window.localStorage.getItem('loginResponse');
  if (login) {
    const obj = JSON.parse(login);
    const t = (new Date(obj.created)).getTime() + 1000 * obj.ttl;
    if (t <= Date.now()) {
      return;
    }
    store.commit('login', obj);
    store.dispatch('setUserInfo', obj).catch((e) => {
      store.commit('logout');
    });
  }
})();

Vue.config.productionTip = false;
Vue.use(Vuetify);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
