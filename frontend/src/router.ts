import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import BotManager from './views/BotManager.vue';
import About from './views/About.vue';
import Documentation from './views/Documentation.vue';
import LoginPage from './views/LoginPage.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/bots',
      name: 'botManager',
      component: BotManager,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/docs',
      name: 'docs',
      component: Documentation,
    },
    {
      path: '/signup',
      name: 'signup',
      component: LoginPage,
    },
  ],
});
