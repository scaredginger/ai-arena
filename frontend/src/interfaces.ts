interface BotRevision {
  testsPassed: boolean;
  code: string;
}

interface Bot {
  name: string;
  played: number;
  kills: number;
  deaths: number;
  assists: number;
  dd: number;
  dt: number;
  id: number;
  userId: number;
  revisions: BotRevision[];
}

export {
  BotRevision,
  Bot,
};
