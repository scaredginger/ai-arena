import Vue from 'vue';
import Vuex from 'vuex';
import { Bot, BotRevision } from './interfaces';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    signedIn: false,
    accessToken: '',
    userData: {
      profile: '',
      firstName: '',
      country: '',
      lastName: '',
      realm: '',
      username: '',
      email: '',
      id: -1,
    },
    bots: [] as Bot[],
  },
  mutations: {
    login(state, data: any) {
      state.accessToken = data.id;
      state.signedIn = true;
    },
    updateBots(state, bots: Bot[]) {
      state.bots = bots;
    },
    updateBot(state, bot: Bot) {
      Vue.set(state.bots, bot.id, bot);
    },
    logout(state) {
      state.userData = {
        profile: '',
        firstName: '',
        country: '',
        lastName: '',
        realm: '',
        username: '',
        email: '',
        id: -1,
      };
      state.accessToken = '';
      state.signedIn = false;
    },
    setUserInfo(state, data: any) {
      state.userData = {
        profile: data.profile ? data.profile : '',
        firstName: data.firstName ? data.firstName : '',
        country: data.country ? data.country : '',
        lastName: data.lastName ? data.lastName : '',
        realm: data.realm ? data.realm : '',
        username: data.username ? data.username : '',
        email: data.email ? data.email : '',
        id: data.id ? data.id : -1,
      };
    },
    setRevisions(state, data) {
      state.bots[data.id].revisions = data.revisions;
    },
  },
  actions: {
    fetchBots(context) {
      if (!context.state.signedIn) {
        throw new Error('Not signed in.');
      }
      const { id } = context.state.userData;
      return fetch(`http://localhost:3000/api/BaseUsers/${id}/bots`)
        .then((res) => res.json())
        .then((obj) => {
          if (obj.error) {
            throw obj.error;
          }
          if (!(obj instanceof Array)) {
            throw TypeError('Expected an array');
          }
          const bots: any = {};
          obj.forEach((bot: any) => {
            bots[bot.id] = bot;
          });
          context.commit('updateBots', bots);
          return obj;
        });
    },
    fetchBotRevisions(context, id: number) {
      if (!context.state.signedIn) {
        throw new Error('Not signed in.');
      }
      const { revisions } = context.state.bots[id];
      if (revisions) {
        return revisions;
      }
      return fetch(`localhost:3000/api/Bots/${id}/BotRevisions?access_token=${context.state.accessToken}`)
        .then((str) => str.json())
        .then((arr) => {
          if (!arr.length) {
            throw new Error('Server returned no revisions.');
          }
          context.commit('setRevisions', {
            id,
            arr,
          });
          return arr;
        });
    },
    setUserInfo(context, params) {
      let error = false;
      return fetch(`http://localhost:3000/api/BaseUsers/${params.userId}?access_token=${params.id}`)
        .then((res) => {
          if (!res.ok) {
            error = true;
            throw res;
          }
          return res.json();
        })
        .then((res) => {
          if (res.error) {
            throw res.error;
          }
          context.commit('setUserInfo', res);
          return res;
        });
    },
    login(context, data) {
      return fetch('http://localhost:3000/api/BaseUsers/login', {
        body: JSON.stringify(data),
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((res) => {
          if (!res.ok) {
            throw res;
          }
          return res.json();
        })
        .then((res) => {
          context.commit('login', res);
          window.localStorage.setItem(
            'loginResponse',
            JSON.stringify(res),
          );
          return context.dispatch('setUserInfo', {
            id: res.id,
            userId: res.userId,
          });
        });
    },
    signup(context, data) {
      return fetch('http://localhost:3000/api/BaseUsers', {
        body: JSON.stringify(data),
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((res) => res.json())
        .then((res) => {
          if (res.success) {
            return context.dispatch('login', {
              email: data.email,
              password: data.password,
            });
          }
          return res;
        });
    },
    logout(context) {
      const token = context.state.accessToken;
      if (!token) {
        return false;
      }
      console.log(token);
      return fetch(`http://localhost:3000/api/BaseUsers/logout?access_token=${token}`, {
        method: 'post',
      }).then((res) => {
        if (!res.ok) {
          throw res;
        }
      }).then(() => {
          window.localStorage.removeItem('loginResponse');
          context.commit('logout');
        });
    },
  },
});
