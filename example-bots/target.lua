local module = {}

local i = 0

local course = {
	newVector(0, 0, 10),
	newVector(0, 10, 0),
	newVector(10, 0, 0),
	-newVector(0, 0, 10),
	-newVector(0, 10, 0),
	-newVector(10, 0, 0),
	newVector(0, 0, 0)
}

function module.tick(info)
	local dest = course[i % 7 + 1]

	local wanted = dest - info.position
	local distance = wanted:length2()
	if distance < 0.25 then
		i = i + 1
	end
	
	local r = info.velocity:getRotation(wanted)
	local thrust = 1

	return {["torque"]=r, ["thrust"]=thrust}
end

function module.start(i)
	return {}
end

return module

