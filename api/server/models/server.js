'use strict';

module.exports = function(Server) {
  Server.disableRemoteMethodByName("upsert");
  Server.disableRemoteMethodByName("find");
  Server.disableRemoteMethodByName("replaceOrCreate");
  Server.disableRemoteMethodByName("create");

  Server.disableRemoteMethodByName("prototype.updateAttributes");
  Server.disableRemoteMethodByName("findById");
  Server.disableRemoteMethodByName("exists");
  Server.disableRemoteMethodByName("replaceById");
  Server.disableRemoteMethodByName("deleteById");

  Server.disableRemoteMethodByName('prototype.__get__accessTokens');
  Server.disableRemoteMethodByName('prototype.__create__accessTokens');
  Server.disableRemoteMethodByName('prototype.__delete__accessTokens');

  Server.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Server.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Server.disableRemoteMethodByName('prototype.__destroyById__accessTokens');

  Server.disableRemoteMethodByName('prototype.__count__accessTokens');

  Server.disableRemoteMethodByName("prototype.verify");
  Server.disableRemoteMethodByName("changePassword");
  Server.disableRemoteMethodByName("createChangeStream");

  Server.disableRemoteMethodByName("confirm");
  Server.disableRemoteMethodByName("count");
  Server.disableRemoteMethodByName("findOne");

  Server.disableRemoteMethodByName("resetPassword");
  Server.disableRemoteMethodByName("setPassword");
  Server.disableRemoteMethodByName("update");
  Server.disableRemoteMethodByName("upsertWithWhere");
};
