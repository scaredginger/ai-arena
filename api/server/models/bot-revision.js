'use strict';

module.exports = function(BotRevision) {
  BotRevision.disableRemoteMethodByName('replaceOrCreate');
  BotRevision.disableRemoteMethodByName('patchOrCreate');
  BotRevision.disableRemoteMethodByName('destroyById');
  BotRevision.disableRemoteMethodByName('replaceById');
  BotRevision.disableRemoteMethodByName('prototype.patchAttributes');
  BotRevision.disableRemoteMethodByName('createChangeStream');
  BotRevision.disableRemoteMethodByName('updateAll');
  BotRevision.disableRemoteMethodByName('replaceOrCreate');
  BotRevision.disableRemoteMethodByName('replaceById');
};
