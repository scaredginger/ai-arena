'use strict';

module.exports = function(BaseUser) {
  const mutableAttrs = {
    firstName: true,
    lastName: true,
  };

  BaseUser.prototype.mutate = function(obj, cb) {
    const keys = Object.keys(obj);
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      if (!mutableAttrs[key]) {
        cb('Updating \'' + key + '\' is not allowed');
        return;
      }
    }
    this.updateAttributes(obj, cb);
  };

  BaseUser.prototype.createBot = function(name, code, cb) {
    this.__create__bots({
      name,
    }, (err, bot) => {
      if (err) {
        cb(err);
        return;
      }
      bot.__create__botRevision({code}, (err, revision) => {
        if (err) {
          cb(err);
          return;
        }
        cb(null, bot);
      });
    });
  };

  BaseUser.disableRemoteMethodByName('upsert');
  BaseUser.disableRemoteMethodByName('find');
  BaseUser.disableRemoteMethodByName('replaceOrCreate');
  // BaseUser.disableRemoteMethodByName('create');

  BaseUser.disableRemoteMethodByName('prototype.updateAttributes');
  // BaseUser.disableRemoteMethodByName('findById');
  BaseUser.disableRemoteMethodByName('exists');
  BaseUser.disableRemoteMethodByName('replaceById');
  BaseUser.disableRemoteMethodByName('deleteById');

  BaseUser.disableRemoteMethodByName('prototype.__get__accessTokens');
  BaseUser.disableRemoteMethodByName('prototype.__create__accessTokens');
  BaseUser.disableRemoteMethodByName('prototype.__delete__accessTokens');

  BaseUser.disableRemoteMethodByName('prototype.__findById__accessTokens');
  BaseUser.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  BaseUser.disableRemoteMethodByName('prototype.__destroyById__accessTokens');

  BaseUser.disableRemoteMethodByName('prototype.__count__accessTokens');

  BaseUser.disableRemoteMethodByName('prototype.verify');
  BaseUser.disableRemoteMethodByName('createChangeStream');

  BaseUser.disableRemoteMethodByName('confirm');
  BaseUser.disableRemoteMethodByName('count');
  BaseUser.disableRemoteMethodByName('findOne');

  BaseUser.disableRemoteMethodByName('resetPassword');
  BaseUser.disableRemoteMethodByName('setPassword');
  BaseUser.disableRemoteMethodByName('update');
  BaseUser.disableRemoteMethodByName('upsertWithWhere');

  BaseUser.remoteMethod('mutate', {
    accepts: [
      {
        arg: 'data',
        type: 'object',
        http: {
          source: 'body',
        },
        required: true,
        root: true,
      },
    ],
    http: {
      path: '/mutate',
      verb: 'POST',
      status: 200,
    },
    isStatic: false,
    returns: {
      type: 'object',
      default: {},
      root: true,
    },
    description: 'Updates attributes for a user',
  });

  BaseUser.remoteMethod('createBot', {
    'accepts': [
      {
        'arg': 'name',
        'type': 'string',
        'http': {
          'source': 'form',
        },
        'required': true,
      },
      {
        'arg': 'code',
        'type': 'string',
        'http': {
          'source': 'form',
        },
        'required': true,
      },
    ],
    isStatic: false,
    'returns': {
      'type': 'object',
      'default': {},
      'root': true,
    },
    'description': 'Inserts a new bot into the user\'s account',
    'http': {
      'path': '/createBot',
      'verb': 'POST',
      'status': 200,
    },
  });
};
