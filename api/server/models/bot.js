'use strict';

module.exports = function(Bot) {
  Bot.disableRemoteMethodByName('create');
  Bot.disableRemoteMethodByName('replaceOrCreate');
  Bot.disableRemoteMethodByName('patchOrCreate');
  Bot.disableRemoteMethodByName('exists');
  // Bot.disableRemoteMethodByName('findById');
  Bot.disableRemoteMethodByName('find');
  Bot.disableRemoteMethodByName('findOne');
  Bot.disableRemoteMethodByName('destroyById');
  Bot.disableRemoteMethodByName('count');
  Bot.disableRemoteMethodByName('replaceById');
  Bot.disableRemoteMethodByName('prototype.patchAttributes');
  Bot.disableRemoteMethodByName('createChangeStream');
  Bot.disableRemoteMethodByName('updateAll');
  Bot.disableRemoteMethodByName('replaceOrCreate');
  Bot.disableRemoteMethodByName('replaceById');
  Bot.disableRemoteMethodByName('upsertWithWhere');
};
