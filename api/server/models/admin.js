'use strict';

module.exports = function(Admin) {
  Admin.disableRemoteMethodByName("upsert");
  Admin.disableRemoteMethodByName("find");
  Admin.disableRemoteMethodByName("replaceOrCreate");
  Admin.disableRemoteMethodByName("create");

  Admin.disableRemoteMethodByName("prototype.updateAttributes");
  Admin.disableRemoteMethodByName("findById");
  Admin.disableRemoteMethodByName("exists");
  Admin.disableRemoteMethodByName("replaceById");
  Admin.disableRemoteMethodByName("deleteById");

  Admin.disableRemoteMethodByName('prototype.__get__accessTokens');
  Admin.disableRemoteMethodByName('prototype.__create__accessTokens');
  Admin.disableRemoteMethodByName('prototype.__delete__accessTokens');

  Admin.disableRemoteMethodByName('prototype.__findById__accessTokens');
  Admin.disableRemoteMethodByName('prototype.__updateById__accessTokens');
  Admin.disableRemoteMethodByName('prototype.__destroyById__accessTokens');

  Admin.disableRemoteMethodByName('prototype.__count__accessTokens');

  Admin.disableRemoteMethodByName("prototype.verify");
  Admin.disableRemoteMethodByName("changePassword");
  Admin.disableRemoteMethodByName("createChangeStream");

  Admin.disableRemoteMethodByName("confirm");
  Admin.disableRemoteMethodByName("count");
  Admin.disableRemoteMethodByName("findOne");

  Admin.disableRemoteMethodByName("resetPassword");
  Admin.disableRemoteMethodByName("setPassword");
  Admin.disableRemoteMethodByName("update");
  Admin.disableRemoteMethodByName("upsertWithWhere");
};
