'use strict';

module.exports = function(server) {
  const {maria} = server.dataSources;
  const tables = [
    'BaseUser',
    'Admin',
    'Server',
    'Bot',
    'BotRevision',
  ];
  maria.autoupdate(tables, (err) => {
    if (err) throw err;
  });
};
