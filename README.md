# AI Arena

A game where lua bots fly planes, trying to shoot each other down in a massive
arena.

The eventual goal is to use websockets to stream a view of live games being
played.

## Status

A great deal of the game functionality has been developed, with internal
interfaces defined and interfaces for the master node's communication with bots.

The front end and data api are still being designed.

## Lua interface

All bots will be written in Lua. This is because Lua is a small, yet flexible
language that's easy to learn and use, but is also performant (with LuaJIT)
and can easily be embedded.

Further advantages are that since Lua integrates nicely with C, methods can be
designed to perform linear algebra on behalf of the user; in particular, to
compute 3D transformations.