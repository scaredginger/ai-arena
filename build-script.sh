cd LuaJIT &&
make &&
make install &&
cd .. &&
git clone https://github.com/bulletphysics/bullet3.git &&
cd bullet3 &&
mkdir cmake_build &&
cd cmake_build &&
cmake -DBUILD_BULLET2_DEMOS:BOOL="0" -DUSE_DOUBLE_PRECISION:BOOL="1" -DUSE_GRAPHICAL_BENCHMARK:BOOL="0" -DUSE_SOFT_BODY_MULTI_BODY_DYNAMICS_WORLD:BOOL="0" -DBUILD_UNIT_TESTS:BOOL="0" -DBUILD_CPU_DEMOS:BOOL="0" -DBUILD_EXTRAS:BOOL="0" -DUSE_GLUT:BOOL="0" -DBUILD_OPENGL3_DEMOS:BOOL="0" -DINSTALL_CMAKE_FILES:BOOL="0" -DINSTALL_EXTRA_LIBS:BOOL="0" -DBUILD_CLSOCKET:BOOL="0" -DBUILD_ENET:BOOL="0" -DCMAKE_COLOR_MAKEFILE:BOOL="0" .. &&
make &&
make install &&
mv /usr/local/include/bullet/* /usr/local/include &&
cd .. &&
cd .. &&
cd ../game &&
mkdir build &&
mkdir bin &&
make
